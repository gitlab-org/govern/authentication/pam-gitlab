package main

// code in here can't be tested because it relies on cgo. :(

import (
	"fmt"
	"log/syslog"
	"strings"
	"unsafe"
)

/*
#cgo LDFLAGS: -lpam -fPIC
#include <security/pam_appl.h>
#include <stdlib.h>
#include <string.h>

char *string_from_argv(int, char**);
char *get_user(pam_handle_t *pamh);
int get_uid(char *user);
static int converse(pam_handle_t *pamh, int nargs,
                    const struct pam_message **message,
                    struct pam_response **response) {
  struct pam_conv *conv;
  int retval = pam_get_item(pamh, PAM_CONV, (void *)&conv);
  if (retval != PAM_SUCCESS) {
    return retval;
  }
  return conv->conv(nargs, message, response, conv->appdata_ptr);
}

static char *request_pass(pam_handle_t *pamh, int echocode,
                           char *prompt) {
  // Query user for verification code
  const struct pam_message msg = { .msg_style = echocode,
                                   .msg       = prompt };
  const struct pam_message *msgs = &msg;
  struct pam_response *resp = NULL;
  int retval = converse(pamh, 1, &msgs, &resp);
  char *ret = NULL;
  if (retval != PAM_SUCCESS || resp == NULL || resp->resp == NULL ||
      *resp->resp == '\000') {
    if (retval == PAM_SUCCESS && resp && resp->resp) {
      ret = resp->resp;
    }
  } else {
    ret = resp->resp;
  }

  // Deallocate temporary storage
  if (resp) {
    if (!ret) {
      free(resp->resp);
    }
    free(resp);
  }

  return ret;
}

static const char *get_ssh_user_info(pam_handle_t * pamh){
	const char * ssh_user_info;

	ssh_user_info = pam_getenv(pamh, "SSH_AUTH_INFO_0");
	if (ssh_user_info == NULL) {
		ssh_user_info = pam_getenv(pamh, "SSH_USER_AUTH");
		if (ssh_user_info == NULL) {
			return NULL;
		}
	}
	if (strlen(ssh_user_info) == 0) {
		return NULL;
	}
	return ssh_user_info;
}

*/
import "C"

func pamLog(format string, args ...interface{}) {
	l, err := syslog.New(syslog.LOG_AUTH|syslog.LOG_WARNING, "pam-module")
	if err != nil {
		return
	}
	l.Warning(fmt.Sprintf(format, args...))
}

func init() {
	if !disablePtrace() {
		pamLog("unable to disable ptrace")
	}
}

func sliceFromArgv(argc C.int, argv **C.char) []string {
	r := make([]string, 0, argc)
	for i := 0; i < int(argc); i++ {
		s := C.string_from_argv(C.int(i), argv)
		defer C.free(unsafe.Pointer(s))
		r = append(r, C.GoString(s))
	}
	return r
}

func request_pass(sshKeyId string, pamh *C.pam_handle_t) string {
	return C.GoString(C.request_pass(pamh, C.PAM_PROMPT_ECHO_ON, C.CString("Gitlab OTP: ")))
}

func get_ssh_key_id(pamh *C.pam_handle_t) string {
	ssh_user_auth := C.GoString(C.get_ssh_user_info(pamh))

	entry := strings.Split(strings.Split(ssh_user_auth, ",")[0], " ")
	keyEntry := strings.Split(entry[len(entry)-1], ":")

	return keyEntry[len(keyEntry)-1]
}

//export pam_sm_authenticate
func pam_sm_authenticate(pamh *C.pam_handle_t, flags, argc C.int, argv **C.char) C.int {
	sshKeyId := get_ssh_key_id(pamh)
	api, err := newAPIClient()

	if err != nil {
		return C.PAM_AUTH_ERR
	}

	requestOtp, err := api.GetConfig(sshKeyId)
	if err != nil {
		return C.PAM_AUTH_ERR
	}

	if requestOtp {
		passotp := request_pass(sshKeyId, pamh)
		r := authenticate(api, sshKeyId, passotp)

		if r == AuthError {
			return C.PAM_AUTH_ERR
		}
	}

	// Return success for cases in which we bypass the OTP check
	return C.PAM_SUCCESS
}

//export pam_sm_setcred
func pam_sm_setcred(pamh *C.pam_handle_t, flags, argc C.int, argv **C.char) C.int {
	return C.PAM_IGNORE
}
